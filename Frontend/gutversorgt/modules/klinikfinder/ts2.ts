subpage = PAGE
subpage.10 = TEMPLATE
subpage.10.template = FILE
subpage.10.template.file = fileadmin/template/index-sub.html
subpage.shortcutIcon = fileadmin/template/favicon.ico
subpage.includeCSS {
  file1 = fileadmin/template/css/custom.css
  file2 = fileadmin/tuerkei/modules/klinikfinder/style.css
}
subpage.stylesheet = fileadmin/template/css/responsive.css
subpage.10.workOnSubpart = DOCUMENT
subpage.headerData.100 = TEXT
subpage.headerData.100.value (
	<script src="fileadmin/template/modules/klinikfinder/js/jquery.zoom-min.js" type="text/javascript"></script>
	<script src="fileadmin/template/modules/klinikfinder/js/jquery.min.js" type="text/javascript"></script>
)
######################################################
#
# Configuration of SUBPARTS
#
######################################################

# Define the subparts, which are inside the subpart DOCUMENT
subpage.10.subparts {
  METANAV = HMENU
  METANAV.wrap = <ul>|</ul>
  METANAV.special = list
  METANAV.special.value = 1162, 1161
  METANAV.1 = TMENU
  METANAV.1 {
          NO = 1
          NO {
                  allWrap = <li>|</li><span>&#124;</span>
          }
  }


  MAINNAV = HMENU
  MAINNAV.wrap = <ul>|</ul>
  MAINNAV.special = list
  MAINNAV.special.value = 1163, 1158, 1159, 1160
  MAINNAV.1 = TMENU
  MAINNAV.1 {
        NO = 1
        NO {
                allWrap = <li class="col-s-3 col-xs-6 col-xxs-12"><a>|</li>
        }

        ACT = 1
        ACT {
                allWrap = <li class="current col-s-3 col-xs-6 col-xxs-12">|</li>
        }
  }

  FIRSTCONTENT = CONTENT
  FIRSTCONTENT < styles.content.get
  HEADERH2 = CONTENT
  HEADERH2 < styles.content.getLeft
  SECONDCONTENT = CONTENT
  SECONDCONTENT < styles.content.getRight
}

######################################################
#
# Configuration of MARKS
#
######################################################

# Define the marks inside the subpart DOCUMENT
subpage.10.marks {
##############################################
#
# Mark BREADCRUMB
#
##############################################

# Outputs a menu which shows a click path to
# the current page.
FOOTNAV = HMENU

FOOTNAV {
        special = rootline
        special.range = 0|-1

        1 = TMENU
        1 {
                NO = 1
                NO.allWrap = ||&nbsp;<span>?</span>&nbsp;
        }
}
}?