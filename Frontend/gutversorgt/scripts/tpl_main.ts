# Default PAGE object:
subpage = PAGE
subpage.10 = TEMPLATE
subpage.10.template = FILE
subpage.10.template.file = fileadmin/gvidt/index.html
subpage.shortcutIcon = fileadmin/gvidt/favicon.ico
# Seitentitel entfernen
config.noPageTitle = 1
subpage {
  # Neuen title Tag in headerData setzen
  headerData {
    10 = TEXT
    10.value = <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    20 = TEXT
    20 {
      field = title
      noTrimWrap = |<title>| - in der T�rkei!</title>|
    }
  }
}
subpage.includeCSS {
  file1 = fileadmin/gvidt/css/custom.css
}
subpage.stylesheet = fileadmin/gvidt/css/responsive.css
subpage.10.workOnSubpart = DOCUMENT
######################################################
#
# Configuration of SUBPARTS
#
######################################################

# Define the subparts, which are inside the subpart DOCUMENT
subpage.10.subparts {
  METANAV = HMENU
  METANAV.wrap = <ul>|</ul>
  METANAV.special = list
  METANAV.special.value = 1162, 1161
  METANAV.1 = TMENU
  METANAV.1 {
          NO = 1
          NO {
                  allWrap = <li>|</li><span>&#124;</span>
          }
  }


  MAINNAV = HMENU
  MAINNAV.wrap = <ul>|</ul>
  MAINNAV.special = list
  MAINNAV.special.value = 1163, 1158, 1159, 1160
  MAINNAV.1 = TMENU
  MAINNAV.1 {
        NO = 1
        NO {
                allWrap = <li class="col-s-3 col-xs-6 col-xxs-12"><a>|</li>
        }

        ACT = 1
        ACT {
                allWrap = <li class="current col-s-3 col-xs-6 col-xxs-12">|</li>
        }
  }

  FIRSTCONTENT = CONTENT
  FIRSTCONTENT < styles.content.get
  HEADERH2 = CONTENT
  HEADERH2 < styles.content.getLeft
  SECONDCONTENT = CONTENT
  SECONDCONTENT < styles.content.getRight
}

######################################################
#
# Configuration of MARKS
#
######################################################

# Define the marks inside the subpart DOCUMENT
subpage.10.marks {
HEADERIMG = TEXT
HEADERIMG.value = gutversorgt


##############################################
#
# Mark BREADCRUMB
#
##############################################

# Outputs a menu which shows a click path to
# the current page.
FOOTNAV = HMENU

FOOTNAV {
        special = rootline
        special.range = 0|-1

        1 = TMENU
        1 {
                NO = 1
                NO.allWrap = ||&nbsp;<span>?</span>&nbsp;
        }
}
}?