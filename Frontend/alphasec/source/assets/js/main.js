(function () {
    
    // hasClass
    function hasClass(elem, className) {
        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
    }

    // toggleClass
    function toggleClass(elem, className) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(' ' + className + ' ') >= 0) {
                newClass = newClass.replace(' ' + className + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        } else {
            elem.className += ' ' + className;
        }
    }

    // Mobile nav function
    var mobileNav = document.querySelector('.nav-mobile');
    var toggle = document.querySelector('.b-nav--10');
    mobileNav.onclick = function () {
        toggleClass(this, 'btn-link-active');
        toggleClass(toggle, 'nav-active');
    };
})();
// Google Maps

var map;

var styleArray = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#e74c3c"
            },
            {
                "visibility": "on"
            }
        ]
    }
];
function initMap() {
  var WhereIsAS = {lat: 51.31878750000001, lng: 9.494232600000032};
  map = new google.maps.Map(document.getElementById('map'), {
    center: WhereIsAS,
    zoom: 13,
    scrollwheel: false,
    styles: styleArray,
    mapTypeControl: false,
    draggable: false,
    streetViewControl: true,
    zoomControl: true
  });
  //Productive path. Bei Mehraufkommen zentrale Variable bereitstellen und so schnell switchen können.
  var image = '../site/templates/assets/img/pin_as.png';
  marker = new google.maps.Marker({
    map: map,
    position: WhereIsAS,
    title: 'AlphaSec Sicherheit 7/24 GmbH, Werner-Hilpert-Straße 10-12, 34117 Kassel',
    icon: image
  });
}
/*global $ */
$(document).ready(function () {

    "use strict";

    $('.b-nav--10 > ul > li:has( > ul)').addClass('is-dropdown');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI


    $('.b-nav--10 > ul > li > ul:not(:has(ul))').addClass('normal-sub');
    //Checks if drodown menu's li elements have anothere level (ul), if not the dropdown is shown as regular dropdown, not a mega menu (thanks Luka Kladaric)

    //Mobile menu is hidden if width is more then 959px, but normal menu is displayed
    //Normal menu is hidden if width is below 959px, and jquery adds mobile menu
    //Done this way so it can be used with wordpress without any trouble

    $(".b-nav--10 > ul > li").hover(function (e) {
        if ($(window).width() > 943) {
            $(this).children("ul").stop(true, false).fadeToggle(150);
            $(this).toggleClass('current-hover');
             $(this).children("a.u-mobile-visible").remove();
            e.preventDefault();
        }
    });
    //If width is more than 943px dropdowns are displayed on hover
    $(".b-nav--10 > ul > li").click(function (e) {
        if ($(window).width() <= 943) {
            $(this).children(".u-mobile-visible").fadeToggle(150);
            $(this).children("ul").fadeToggle(150);
            if($(this).has('ul').length){ //If no submenu, no prevent on click
                e.preventDefault();
            }
            if(!$(this).children(".u-mobile-visible").length && $(this).find('ul:not(:has(ul))').length){
                $(this).children("a").after(function() { // If there's no link yet and if it's a submenu, add link
                    return "<a class='u-mobile-visible ' href='" + this.href + "'>Seite besuchen</a>"; 
                });
            }
            $(this).children("a.u-mobile-visible").click(function(){ //If "visit page"-link is clicked, let it redirect. Build fallback for if no or very short link (what if a top-li has no link?)
                    $(this).parent().unbind("click");
            });
            $(this).children("ul").children("li").click(function(){ //If sub-child is clicked, let it redirect. Build fallback for if no or very short link (what if a top-li has no link?)
                    $(this).parent().parent().unbind("click");
            });
        }
    });
    //If width is less or equal to 943px dropdowns are displayed on click (thanks Aman Jain from stackoverflow)

    //HelpCTA toggle function
    $(".l-helpcta--10").show(); // Make sure this only shows with JS - css is display none
    $(".x-helpcta-select").change(function(){
        var newval = $(this).val(); // val of option of select
        $(".x-helpcta-unit").slideUp(); // hide all open units
        $("[data-trigger='"+newval+"']").slideToggle(); // toggle this unit
    });
});

// ToTop Plugin
/*
var defaults = {
  containerID: 'toTop', // fading element id
containerHoverID: 'toTopHover', // fading element hover id
scrollSpeed: 1200,
easingType: 'linear' 
 };
*/
$(document).ready(function() {
$().UItoTop({ easingType: 'easeOutQuart' });
});    
