'use strict';

/* Plugins & vars
 * -------------------- */
// Main dependencies
var gulp                  = require('gulp');
var browserSync           = require('browser-sync').create();
var path                  = require('path');

// build:html dependencies
var nunjucksRender      = require('gulp-nunjucks-render');
var htmlmin             = require('gulp-htmlmin');
var data                = require('gulp-data');

// build:css dependencies
var sass                = require('gulp-sass');
var autoprefixer        = require('gulp-autoprefixer');
var cleancss            = require('gulp-clean-css');
var sassOptions         = {
                            errorLogToConsole: true,
                            outputStyle: 'expanded'
                          };
var autoprefixerOptions = {
                            browsers: ['last 2 versions', '> 5%']
                          };

// build:js dependencies
var concat =              require('gulp-concat');
var uglify =              require('gulp-uglify');

// build:asset dependencies

// build:wordpress_css dependencies
var rename =              require("gulp-rename");

/* Task for browsersync
 * -------------------- */
gulp.task('serve', function() {
                          browserSync.init({
                            server: "./dist",
                            ghostMode: false,
                            logFileChanges: true,
                            open: false
                          });
                          gulp.watch("./source/**/*.nunjucks", ['watch:html']);
                          gulp.watch("./source/**/*.scss", ['watch:css']);
                          gulp.watch("./source/**/*.json", ['watch:html']);
                          gulp.watch("./source/**/*.js", ['watch:js']);
});

/* Task for building nunjucks-Template
 * -------------------- */
gulp.task('build:html', function() {
                          nunjucksRender.nunjucks.configure(['./source/']);
                          // De-caching for Data files
                          function requireUncached( $module ) {
                            delete require.cache[require.resolve( $module )];
                            return require( $module );
                          }
                          // Getting all template-files, excluding all with _underscore (blocks or layout modules)
                          return gulp.src(['./source/**/*.nunjucks', '!./source/**/_*'])  // old: return gulp.src('./source/**/*.+(html|nunjucks)')
                          // Gets _base.json files for overall settings like navigation
                          .pipe(data(function(file) {
                            var jsonfile0 = './source/';
                            return requireUncached(jsonfile0 + '_base.json');
                          }))
                          // Extension-variable to parse as .html - or maybe .php
                          .pipe(nunjucksRender({
                            ext: '.html'
                          }))
                          .pipe(gulp.dest('./dist/'))
                          .pipe(browserSync.stream());
});
gulp.task('min:html', ['build:html'], function() {
                          return gulp.src('./dist/**/*.html')
                            .pipe(htmlmin({
                              collapseWhitespace: true,
                              conservativeCollapse: true
                            }))
                            .pipe(gulp.dest('./dist'));
});
gulp.task('watch:html', ['build:html'], function () {
                          browserSync.reload();
                          //gulp.watch('./source/**/*.nunjucks', ['build:html']);  // Leaving here in case I want a watch-only watch-task
});
/* Task for building sass/css-files
 * -------------------- */
gulp.task('build:css', function() {
                        // Including the main scss files
                        return gulp.src('./source/assets/css/*.scss')
                        .pipe(sass(sassOptions).on('error', sass.logError))
                        // Trigger autoprefixer
                        .pipe(autoprefixer(autoprefixerOptions))
                        // Output the css
                        .pipe(gulp.dest('./dist/assets/css/'))
                        .pipe(browserSync.stream());
});
gulp.task('min:css', ['build:css'], function() {
                        return gulp.src('./dist/assets/css/*.css')
                        .pipe(cleancss({compatibility: 'ie8'}))
                        .pipe(gulp.dest('./dist/assets/css/'));
});
gulp.task('watch:css', ['build:css'], function () {
                        browserSync.reload();
                        //gulp.watch('./source/**/*.scss', ['build:css']); // Leaving here in case I want a watch-only watch-task
});

/* Task for building js-files
 * -------------------- */
gulp.task('build:js', function() {
                        var js_files = ['./source/assets/js/*.js', './source/assets/js/vendor/responsive.min.js', './source/assets/js/vendor/jquery.ui.totop.min.js', './source/assets/js/vendor/jquery.easing-1.3.js'];
                        return gulp.src(js_files)
                        .pipe(concat('main.js'))
                        .pipe(gulp.dest('./dist/assets/js/'));
});
gulp.task('min:js', ['build:js'], function() {
                        return gulp.src('./dist/assets/js/main.js')
                          .pipe(uglify())
                          .pipe(gulp.dest('./dist/assets/js/'));
});
gulp.task('watch:js', ['build:js'], function () {
                        browserSync.reload();
                        //gulp.watch('./source/js/*.js', ['build:js']); // Leaving here in case I want a watch-only watch-task
});
/* Task for building assets
 * -------------------- */
/* Tasks for moving files
  * ------------------- */
gulp.task('move:files', function() {
                    //Root-Folder files
                    gulp.src(['./robots.txt', './humans.txt', './browserconfig.xml', './manifest.json', './crossdomain.xml', './.htaccess'])
                    .pipe(gulp.dest('./dist/'));
                    //Fonts
                    gulp.src(['./source/assets/font/*'])
                    .pipe(gulp.dest('./dist/assets/font/'));
                    //Bilder
                    gulp.src(['./source/assets/img/*'])
                    .pipe(gulp.dest('./dist/assets/img/'));
});
/* Task for building alltogether
 * -------------------- */
gulp.task('build', ['min:html', 'min:css', 'min:js', 'move:files'], function() {
});


/* WP-Builder 
* --------------------- */
gulp.task('build:wordpress', function() {
                          nunjucksRender.nunjucks.configure(['./source/_wordpress/']);
                          // De-caching for Data files
                          function requireUncached( $module ) {
                            delete require.cache[require.resolve( $module )];
                            return require( $module );
                          }
                          return gulp.src(['./source/_wordpress/**/*.nunjucks'])  // old: return gulp.src('./source/**/*.+(html|nunjucks)')
                          // Extension-variable to parse as .php
                          .pipe(nunjucksRender({
                            ext: '.php'
                          }))
                          .pipe(gulp.dest('./wordpress_dist/'))
                          .pipe(browserSync.stream());
});
gulp.task('min:wordpress', ['build:wordpress'], function() {
                          return gulp.src('./wordpress_dist/**/*.php')
                            .pipe(htmlmin({
                              collapseWhitespace: true,
                              conservativeCollapse: true
                            }))
                            .pipe(gulp.dest('./wordpress_dist'));
});
gulp.task('build:wordpress_css', function() {
                        // Including the main scss files
                        return gulp.src('./source/assets/css/*.scss')
                        .pipe(sass(sassOptions).on('error', sass.logError))
                        // Trigger autoprefixer
                        .pipe(autoprefixer(autoprefixerOptions))
                        .pipe(rename("style.css"))
                        // Output the css
                        .pipe(gulp.dest('./wordpress_dist/'))
                        .pipe(browserSync.stream());
});
gulp.task('min:wordpress_css', ['build:wordpress_css'], function() {
                        return gulp.src('./wordpress_dist/style.css')
                        .pipe(cleancss({compatibility: 'ie8'}))
                        .pipe(gulp.dest('./wordpress_dist/'));
});
gulp.task('move:wordpress_files', function() {
                    //Root-Folder files
                    gulp.src(['./robots.txt', './humans.txt', './browserconfig.xml', './manifest.json', './crossdomain.xml'])
                    .pipe(gulp.dest('./wordpress_dist/'));
                    //Fonts
                    gulp.src(['./source/assets/font/*'])
                    .pipe(gulp.dest('./wordpress_dist/assets/font/'));
});
gulp.task('build_wordpress', ['min:wordpress', 'min:wordpress_css', 'move:wordpress_files'], function() {
});
/* Processwire-Builder
* --------------------- */
gulp.task('build:processwire', function() {
                          nunjucksRender.nunjucks.configure(['./source/_processwire/']);
                          // De-caching for Data files
                          function requireUncached( $module ) {
                            delete require.cache[require.resolve( $module )];
                            return require( $module );
                          }
                          return gulp.src(['./source/_processwire/**/*.nunjucks'])  // old: return gulp.src('./source/**/*.+(html|nunjucks)')
                          // Extension-variable to parse as .php
                          .pipe(nunjucksRender({
                            ext: '.php'
                          }))
                          .pipe(gulp.dest('./processwire_dist/'))
                          .pipe(browserSync.stream());
});
gulp.task('min:processwire', ['build:processwire'], function() {
                          return gulp.src('./processwire_dist/**/*.php')
                            .pipe(htmlmin({
                              collapseWhitespace: true,
                              conservativeCollapse: true
                            }))
                            .pipe(gulp.dest('./processwire_dist'));
});