# Design

"Auf die Schnelle" eine PDF mit einigen Logos und Brandingansätzen für Auftraggeber aus der mittelfristigen Vergangenheit

---

# Frontend

## 2016 - gutversorgt - http://www.gut-versorgt-in-der-tuerkei.de

Eine Infopage für eine Versicherungsseite der Barmenia, die mit Typo3 umgesetzt werden sollte. (Die Vorgängerseite lief schon mit Typo3)  

Meine Aufgabe war hauptsächlich das Aufsetzen eine responsiven neuen Designs in Anlehnung an den Styleguide sowie der Hauptseite der Versicherung (https://www.viactiv.de)

### Gedanken dazu:
Bis dahin hatte ich keine Erfahrungen mit TypoScript, habe die Zusammenhänge nach einer Einarbeitung in die Zusammenhänge und Logik des Backends aber durchschauen können und via Dokumentation und Google Lösungen hergeleitet.  
Immer nach dem autodidaktischen Motto "alles ist realisierbar".  

Leider ist die Seite mittlerweile nicht mehr zu 100% in Ordnung, z.B. scheint es Probleme bei der utf-Formatierung bei den Daten aus dem Backend geben, außerdem ist kein SSL geschaltet. Das liegt derzeit aber leider außerhalb meines Machtbereichs.

### Technik/Details
Typo3 4.4  
Als CSS-Framework hatte ich http://responsivebp.com genutzt. Schnell und Designneutral einzusetzen, lightweight und ein gutes Grid-System.

### Demo
http://www.gut-versorgt-in-der-tuerkei.de  


//  


## 2017 - alphasec - https://alphasec-sicherheit.de/ (Demo ist unten)

Noch nicht gelaunched - Frontend aber komplett

### Gedanken dazu:
Dieser Seite liegt einer meiner erarbeiteten Frontend-Workflows zugrunde, mehr dazu in den Details und im Code zu sehen.  
Dabei arbeite ich mit frei nach "Don't repeat yourself" mit modularen Blöcken, die ich über die Zeit kreiere und füge sie dann je nach Aufbau, Bedarf und Umfang zusammen.  
Mittlerweile ist mein Stack umfangreicher und vereinfachter als bei diesem Projekt.

### Technik/Details
Backend: Processwire (die Implementierung in das CMS ist im Code nicht zu sehen, dort findet sich nur das Frontend, MySQL  
Frontend: ResponsiveBP, Eigenes  
Building: Gulp, SCSS, HTML5, Nunjucks (Template-Engine)

### Demo
https://alphasec-sicherheit.de/pw/backend/  
Benutzer: demo  
Passwort: u33iKLxPWz2C    

Eingeloggt ist die Seite unter https://alphasec-sicherheit.de/pw/ sichtbar. (Noch nicht alle Funktionen und Inhalte eingepflegt)

---

# Front- und Backend

## 2017 - shehe_trials - Einfache Probemitgliederverwaltung für ein Fitnessstudio

Das städtische Fitnessstudio hat etwas Hilfe bei der Verwaltung benötigt und hat von mir diese kleine Suite erhalten.  

Mit diesem Tool können Mitarbeiter am Empfang ihre Probemitglieder erfassen, prüfen und verwalten.

### Gedanken dazu:
Da hier eine schnelle Lösung Sinn gemacht hat, habe ich auf Laravel im Backend und VueJS im Frontend gesetzt. Als Framework dafür war mit Vuetify ein schnelles und modernes Interface (Material Design) angelegt was für jeden Benutzbar ist. 


### Technik/Details
Backend: Laravel (API), MySQL  
Frontend: VueJS, Vuetify, axios  
Building: Webpack, phpunit, SCSS

### Demo
Die Daten sind selbstverständlich alle geseeded (https://github.com/fzaninotto/Faker)  
[![Demo anschauen](https://bytebucket.org/Dari4sho/coderep/raw/985305d72affb6f0fca5ebc0b65eacfcdad5b55d/Front-%20und%20Backend/shehe_trials_demo.gif)](https://bitbucket.org/Dari4sho/coderep/raw/985305d72affb6f0fca5ebc0b65eacfcdad5b55d/Front-%20und%20Backend/shehe_trials_demo.webm)

---

Andere Seiten von mir, die gerade so rumschwirren und ich vorzeigen darf:  
https://bossgamma.de - Meine Agenturseite, vor schon längerer Zeit erstellt und Statisch  
http://cgc-consulting.net - WordPress, etwas älter
