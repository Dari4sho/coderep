<?php

use Illuminate\Database\Seeder;

class TrialsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Trial::class, 10)->create();
    }
}
