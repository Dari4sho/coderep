<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trials', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('anrede')->nullable();
            $table->string('name');
            $table->string('vorname');
            $table->string('anschrift')->nullable();
            $table->string('plz')->nullable();
            $table->string('stadt')->nullable();
            $table->string('prtr_von');
            $table->string('prtr_bis');
            $table->boolean('bezahlt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trials');
    }
}
