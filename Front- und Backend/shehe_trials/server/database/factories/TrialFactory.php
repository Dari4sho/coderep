<?php
use Faker\Generator as Faker;

$factory->define(App\Trial::class, function (Faker $faker) {
    return [
        'anrede' => rand(1,0),
        'name' => $faker->unique()->lastName,
        'vorname' => $faker->firstName,
        'anschrift' => $faker->streetAddress,
        'plz' => $faker->postcode,
        'stadt' => $faker->city,
        'prtr_von' => $faker->date,
        'prtr_bis' => $faker->date,
        'bezahlt' => rand(1,0)
    ];
});