<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trial;
use App\Http\Resources\Trial as TrialResource;

class TrialController extends Controller
{
    /**
     * Show all trials.
     * 
     * @return Response
     */
    public function index()
    {
        // Get all trials
        $trials = Trial::all(); // Trial::paginate(20);

        // Return a collection of $trial with pagination
        return TrialResource::collection($trials);
    }

    public function show($id)
    {
        // Get the trial
        $trial = Trial::findOrfail($id);

        // Return a single trial
        return new TrialResource($trial);
    }

    public function destroy($id)
    {
        // Get the trial
        $trial = Trial::findOrfail($id);

        // Delete the trial
        if($trial->delete()){
            return new TrialResource($trial);
        }
    }

    public function store(Request $request) {
        $trial = $request->isMethod('put') ? Trial::findOrfail($request->trial_id) : new Trial;

        $trial->anrede = $request->input('anrede');
        $trial->name = $request->input('name');
        $trial->vorname = $request->input('vorname');
        $trial->anschrift = $request->input('anschrift', false);
        $trial->plz = $request->input('plz');
        $trial->stadt = $request->input('stadt');
        $trial->prtr_von = $request->input('prtr_von');
        $trial->prtr_bis = $request->input('prtr_bis');
        $trial->bezahlt = $request->input('bezahlt');

        if($trial->save()) {
            return new TrialResource($trial);
        }
    }
}
