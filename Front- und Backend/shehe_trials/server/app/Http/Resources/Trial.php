<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Trial extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'anrede' => $this->anrede,
            'name' => $this->name,
            'vorname' => $this->vorname,
            'anschrift' => $this->anschrift,
            'plz' => $this->plz,
            'stadt' => $this->stadt,
            'prtr_von' => $this->prtr_von,
            'prtr_bis' => $this->prtr_bis,
            'bezahlt' => $this->bezahlt
        ];
    }
}
