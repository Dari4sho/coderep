<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Get list of trials
Route::get('trials', 'TrialController@index');

// Get specific trial
Route::get('trial/{id}', 'TrialController@show');

// Update existing trial
Route::put('trial', 'TrialController@store');

// Create new trial
Route::post('trial', 'TrialController@store'); 

// Delete as trial
Route::delete('trial/{id}', 'TrialController@destroy');