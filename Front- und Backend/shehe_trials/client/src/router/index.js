import Vue from 'vue'
import Router from 'vue-router'
import Probemitglieder from '@/components/Probemitglieder'
import e404 from '@/components/errors/404'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      name: '404',
      component: e404
    },
    {
      path: '/',
      name: 'Probemitglieder',
      component: Probemitglieder
    }
  ]
})
